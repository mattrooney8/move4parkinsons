var gulp = require('gulp');
var uncss = require('gulp-uncss');
var cleanCSS = require('gulp-clean-css');


gulp.task('uncss', function() {
  return gulp.src([
      'css/style.css'
    ])
    .pipe(uncss({
      html: [
        'http://localhost/CharityHack/',
        'http://localhost/CharityHack/users',
        'http://localhost/CharityHack/user/add',
        'http://localhost/CharityHack/members',
        'http://localhost/CharityHack/member/add',
        'http://localhost/CharityHack/classes',
        'http://localhost/CharityHack/class/add',
        'http://localhost/CharityHack/attendance',
        'http://localhost/CharityHack/attendance/ongoing/3/2017-01-24',
        'http://localhost/CharityHack/attendance/5/2',
        'http://localhost/CharityHack/reports',
        'http://localhost/CharityHack/reports/ongoing/2016/12/3',
        'http://localhost/CharityHack/reports/5'
      ],
        ignore: [/\w\.in/,
                    ".fade",
                    ".collapse",
                    ".collapsing",
                    /(#|\.)navbar(\-[a-zA-Z]+)?/,
                    /(#|\.)dropdown(\-[a-zA-Z]+)?/,
                    /(#|\.)(open)/,
                    ".modal",
                    ".modal.fade.in",
                    ".modal-dialog",
                    ".modal-document",
                    ".modal-scrollbar-measure",
                    ".modal-backdrop.fade",
                    ".modal-backdrop.in",
                    ".modal.fade.modal-dialog",
                    ".modal.in.modal-dialog",
                    ".modal-open",
                    ".in",
                    ".modal-backdrop"]
    }))
    .pipe(gulp.dest('css/'));
});


gulp.task('default', function () {
  return gulp.src('css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist'));
});