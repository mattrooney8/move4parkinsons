<?php

class Subscribers extends AuthenticatedController
{
    const STATUS_PERSON_WITH = '0';
    const STATUS_FAMILY_MEMBER = '1';
    const STATUS_CARER = '2';
    const STATUS_HEALTH_PROFESSIONAL = '3';
    const STATUS_INTERESTED = '4';
    const STATUS_WANT_TO_BE_UPDATED = '5';

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    static $statusList = Array(
        Array(
            'key' => self::STATUS_PERSON_WITH,
            'value' => "I am a person with Parkinson's"
        ),
        Array(
            'key' => self::STATUS_FAMILY_MEMBER,
            'value' => "I am a family member of someone with Parkinson's"
        ),
        Array(
            'key' => self::STATUS_CARER,
            'value' => "I am a carer of someone with Parkinson's"
        ),
        Array(
            'key' => self::STATUS_HEALTH_PROFESSIONAL,
            'value' => "I am a health professional"
        ),
        Array(
            'key' => self::STATUS_INTERESTED,
            'value' => "I'm just interested in Move4Parkinsons"
        ),
        Array(
            'key' => self::STATUS_WANT_TO_BE_UPDATED,
            'value' => "I would like to receive updates from Move4Parkinsons"
        )
    );

    function pageList($f3)
    {
        //Get member Table
        $subscribers = new DB\SQL\Mapper($f3->get('DB'), 'subscribers');
        //Query all subscribers
        $subs = $subscribers->find(array(), array('order' => 'firstname ASC'));
        // $inactive = $subscribers->find(array('status=?', self::STATUS_INACTIVE), array('order' => 'firstname ASC'));
        // Set results as variable to pass to template
        $f3->set('subs', $subs);
        $f3->set('title','Subscribers Manager');
        $f3->set('activenav', 'navsubscribers');
        $f3->set('listfilter', 'active');
        echo \Template::instance()->render('subscribers/list.html');
    }

    function pageAdd($f3)
    {
        // Add record just render template.
        $f3->set('memberStatusList', self::$statusList);
        $f3->set('activenav', 'navsubscribers');
        $f3->set('validate','active');
        $f3->set('title','Add a new Member');

        echo \Template::instance()->render('subscribers/add.html');
    }

    function pageEdit($f3)
    {
        // Get the ID value from URL
        $id = $f3->get('PARAMS.id');
        // Map the DB member table
        $subscriber = new DB\SQL\Mapper($f3->get('DB'), 'subscribers');
        // Run Query to load all values for that row
        $subscriber->load(array('id=?', $id));
        // Copy values to POST
        $subscriber->copyTo('POST');


        $f3->set('memberStatusList', self::$statusList);
        // Render Template
        $f3->set('activenav', 'navsubscribers');
        $f3->set('title','Edit Subscriber');
        $f3->set('validate','active');

        echo \Template::instance()->render('subscribers/edit.html');
    }

    function actionAddUpdate($f3)
    {
        $id = $f3->get('PARAMS.id');
        //create an article object
        $subscriber = new DB\SQL\Mapper($f3->get('DB'), 'subscribers');
        //if we don't load it first Mapper will do an insert instead of update when we use save command
        if ($id) $subscriber->load(array('id=?', $id));

        $subscriber->copyFrom('POST');

        $subscriber->save();
        // Return to admin home page, new blog entry should now be there
        $f3->reroute('/subscribers');
    }

    function status($f3)
    {
        $id = $f3->get('PARAMS.id');
        $member = new DB\SQL\Mapper($f3->get('DB'), 'members');
        $member->load(array('id=?', $id));
        $member->status = $member->status == self::STATUS_ACTIVE ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $member->save();
        $f3->reroute('/members');
    }


    function export($f3)
    {

        $subscribers = $f3->get('DB')->exec('SELECT * FROM subscribers');


        $csv = "\"First Name\",\"Last Name\",\"Email\",\"Address 1\",\"Address 2\",\"Town/City\",\"County\",\"I am\",\"Mobile\",\"Newsletter\"" . PHP_EOL;


        foreach ($subscribers as $key => $value) {

            $status = self::$statusList;

            $firstname = $value['firstname'];
            $lastname = $value['lastname'];
            $email = $value['email'];
            $address1 = $value['address1'];
            $address2 = $value['address2'];
            $city = $value['city'];
            $county = $value['county'];
            $iam = $value['iam'];
            $iamextended = $status[$iam]['value'];
            $mobile = $value['mobile'];
            $newsletter = $value['newsletter'];

            $csv .= "\"$firstname\",\"$lastname\",\"$email\",\"$address1\",\"$address2\",\"$city\",\"$county\",\"$iamextended\",=\"$mobile\",\"$newsletter\"";
            $csv .= PHP_EOL;
        }


        $filename = "Subscriber Data -" . date("d-m-y") . ".csv";
        $exportcsv = new Helper;
        $exportcsv->exportcsv($filename, $csv);

    }

}
