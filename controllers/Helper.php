<?php

class Helper
{

    function exportcsv($filename, $data)
    {
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $data;
        exit();
    }

    static public function renderdate($args)
    {

      $attr = $args['@attrib'];
      $date=$attr['data'];

      $tmpl = \Preview::instance();
      $resolveddate=$tmpl->resolve($date);

      return date_format(date_create($resolveddate),'d/m/Y');

    }

    static public function getMenuItems($f3){
        $useremail = $_SESSION['email'];
        $user=new DB\SQL\Mapper($f3->get('DB'),'users');
        $user->load(array('email=?',$useremail));
        $usertype = $user->role;
        if($usertype == "instructor"){
            $_SESSION['menuitems'] = ["attendance","classes"];
            return ["attendance","classes"];
        } else {
            $_SESSION['menuitems'] = ["attendance","classes", "reports", "members", "users", "charts"];
            return ["attendance","classes", "reports", "members", "users", "charts"];
        }
    }

    static public function setMenuItems($f3){
        if(!isset($_SESSION['menuitems'])){
            $menuitems = Helper::getMenuItems($f3);
            $f3->set('menuitems', $menuitems);
        } else {
            $f3->set('menuitems', $_SESSION['menuitems']);
        }
    }

}